#include <stdint.h>
#include <stdbool.h>
#include "mouseSvc.h"
#include "app_timer.h"

#include "app_usbd.h"
#include "app_usbd_core.h"
#include "app_usbd_hid_generic.h"
#include "app_error.h"
#include "nrfx_usbd.h"
#include "nrf_drv_clock.h"
#include "app_usbd_hid_types.h"

#include "nrf_clock.h"

static void usbd_user_ev_handler(app_usbd_event_type_t event);
static void hid_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                                app_usbd_hid_user_event_t event);

#define HID_GENERIC_INTERFACE  0U
#define HID_GENERIC_EPIN       NRF_DRV_USBD_EPIN1
#define REPORT_IN_QUEUE_SIZE   1U
#define REPORT_OUT_MAXSIZE     0U
#define REPORT_FEATURE_MAXSIZE 31U
#define HID_GENERIC_EP_COUNT   1U

APP_USBD_HID_GENERIC_SUBCLASS_REPORT_DESC(mouse_desc,
{
    0x05, 0x01,       // USAGE_PAGE (Generic Desktop)
    0x09, 0x02,       // USAGE (Mouse)
    0xa1, 0x01,       // COLLECTION (Application)
    0x85, 0x01,       //   REPORT_ID (1)
    0x09, 0x01,       //   USAGE (Pointer)
    0x05, 0x09,       //   USAGE_PAGE (Button)
    0x19, 0x01,       //   USAGE_MINIMUM (Button 1)
    0x29, 0x08,       //   USAGE_MAXIMUM (Button 8)
    0x15, 0x00,       //   LOGICAL_MINIMUM (0)
    0x25, 0x01,       //   LOGICAL_MAXIMUM (1)
    0x95, 0x08,       //   REPORT_COUNT (8)
    0x75, 0x01,       //   REPORT_SIZE (1)
    0x81, 0x02,       //   INPUT (Data,Var,Abs)
    0x75, 0x08,       //   REPORT_SIZE (8)
    0x95, 0x02,       //   REPORT_COUNT (2)
    0x05, 0x01,       //   USAGE_PAGE (Generic Desktop)
    0x09, 0x30,       //   USAGE (X)
    0x09, 0x31,       //   USAGE (Y)
    0x15, 0x00,       //   LOGICAL_MINIMUM (0)
    0x25, 0x00,       //   LOGICAL_MAXIMUM (0)
    0x81, 0x07,       //   INPUT (Cnst, Var, Rel)
    0x95, 0x01,       //   REPORT_COUNT (1)
    0x75, 0x08,       //   REPORT_SIZE (8)
    0x05, 0x01,       //   USAGE_PAGE (Generic Desktop)
    0x09, 0x38,       //   USAGE (Wheel)
    0x15, 0x81,       //   LOGICAL_MINIMUM (-127)
    0x25, 0x7F,       //   LOGICAL_MAXIMUM (127)
    0x81, 0x06,       //   INPUT (Data, Variable, Relative)
    0x95, 0x01,       //   REPORT_COUNT (1)
    0x75, 0x08,       //   REPORT_SIZE (8)
    0x05, 0x0c,       //   USAGE_PAGE (Consumer devices)
    0x0A, 0x38, 0x02, //   USAGE (AC Pan)
    0x15, 0x81,       //   LOGICAL_MINIMUM (-127)
    0x25, 0x7F,       //   LOGICAL_MAXIMUM (127)
    0x81, 0x06,       //   INPUT (Data, Variable, Relative)
    0xc0              // END_COLLECTION
});

static const app_usbd_hid_subclass_desc_t *reports[] = {&mouse_desc};

APP_USBD_HID_GENERIC_GLOBAL_DEF(hid_mouse,
                                HID_GENERIC_INTERFACE,
                                hid_user_ev_handler,
                                (HID_GENERIC_EPIN),
                                reports,
                                REPORT_IN_QUEUE_SIZE,
                                REPORT_OUT_MAXSIZE,
                                REPORT_FEATURE_MAXSIZE,
                                APP_USBD_HID_SUBCLASS_NONE,
                                APP_USBD_HID_PROTO_GENERIC);

static bool m_report_pending = false;

void mouseSvcInit(void)
{
    static const app_usbd_config_t usbd_config = {
            .ev_state_proc = usbd_user_ev_handler
    };
    ret_code_t ret = app_usbd_init(&usbd_config);
    app_usbd_class_inst_t const *class_instance;
    class_instance = app_usbd_hid_generic_class_inst_get(&hid_mouse);
    ret = app_usbd_class_append(class_instance);
    app_usbd_enable();
    app_usbd_start();
}

void mouseSvcProcess(void)
{
    while (app_usbd_event_queue_process()) {

    }
}

void mouseSvcSend(MouseSvcData *data)
{
    if (m_report_pending) {
        return;
    }

    ret_code_t err_code;
    uint8_t repData[sizeof(MouseSvcData) + 1];
    repData[0] = 1;
    memcpy(&repData[1], (uint8_t *)data, sizeof(MouseSvcData));
    err_code = app_usbd_hid_generic_in_report_set(&hid_mouse, repData, sizeof(repData));
    if (err_code == NRF_SUCCESS) {
        m_report_pending = true;
    }
}

static void usbd_user_ev_handler(app_usbd_event_type_t event)
{
    switch (event)
    {
        case APP_USBD_EVT_DRV_SOF:
            break;
        case APP_USBD_EVT_DRV_RESET:
            m_report_pending = false;
            break;
        case APP_USBD_EVT_DRV_SUSPEND:
            m_report_pending = false;
            app_usbd_suspend_req(); // Allow the library to put the peripheral into sleep mode
            //bsp_board_leds_off();
            break;
        case APP_USBD_EVT_DRV_RESUME:
            m_report_pending = false;
            //bsp_board_led_on(LED_USB_START);
            break;
        case APP_USBD_EVT_STARTED:
            m_report_pending = false;
            //bsp_board_led_on(LED_USB_START);
            break;
        case APP_USBD_EVT_STOPPED:
            app_usbd_disable();
            //bsp_board_leds_off();
            break;
        case APP_USBD_EVT_POWER_DETECTED:
            //NRF_LOG_INFO("USB power detected");
            if (!nrf_drv_usbd_is_enabled())
            {
                app_usbd_enable();
            }
            break;
        case APP_USBD_EVT_POWER_REMOVED:
            //NRF_LOG_INFO("USB power removed");
            app_usbd_stop();
            break;
        case APP_USBD_EVT_POWER_READY:
            //NRF_LOG_INFO("USB ready");
            app_usbd_start();
            break;
        default:
            break;
    }
}

static void hid_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                                app_usbd_hid_user_event_t event)
{
    switch (event)
    {
        case APP_USBD_HID_USER_EVT_OUT_REPORT_READY:
        {
            /* No output report defined for this example.*/
            ASSERT(0);
            break;
        }
        case APP_USBD_HID_USER_EVT_IN_REPORT_DONE:
        {
            m_report_pending = false;
            //hid_generic_mouse_process_state();
            //bsp_board_led_invert(LED_HID_REP_IN);
            break;
        }
        case APP_USBD_HID_USER_EVT_SET_BOOT_PROTO:
        {
            UNUSED_RETURN_VALUE(hid_generic_clear_buffer(p_inst));
            //NRF_LOG_INFO("SET_BOOT_PROTO");
            break;
        }
        case APP_USBD_HID_USER_EVT_SET_REPORT_PROTO:
        {
            UNUSED_RETURN_VALUE(hid_generic_clear_buffer(p_inst));
            //NRF_LOG_INFO("SET_REPORT_PROTO");
            break;
        }
        default:
            break;
    }
}
