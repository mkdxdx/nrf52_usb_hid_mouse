#pragma once

#include <stdint.h>

#pragma pack(push,1)
typedef struct MouseSvcData {
    uint8_t buttons;
    int8_t x;
    int8_t y;
    int8_t scroll;
    int8_t pan;
} MouseSvcData;
#pragma pack(pop)

void mouseSvcInit(void);
void mouseSvcSend(MouseSvcData *data);
void mouseSvcProcess(void);
