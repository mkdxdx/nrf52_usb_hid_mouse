#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "nrf_delay.h"
#include "app_timer.h"
#include "nrfx_clock.h"
#include "nrf_pwr_mgmt.h"
#include "mouseSvc.h"
#include "nrf_drv_clock.h"

static bool readMouse = false;
static volatile bool lfClkStarted = false;

APP_TIMER_DEF(mouseTimer);

static void clockEvtHandler(nrfx_clock_evt_type_t event);
static void timerTimeoutHandler(void *ctx);

int main(void)
{
    // nrf_clock required for usb
    ret_code_t ret = nrf_drv_clock_init();
    APP_ERROR_CHECK(ret);

    // lfclk required for app_timer
    nrf_drv_clock_lfclk_request(NULL);
    while (!nrf_drv_clock_lfclk_is_running()) {}

    mouseSvcInit();
    ret = app_timer_init();
    ret = app_timer_create(&mouseTimer, APP_TIMER_MODE_REPEATED, timerTimeoutHandler);
    ret = app_timer_start(mouseTimer, APP_TIMER_TICKS(10), NULL);
    while (true)
    {
        if (readMouse) {
            readMouse = false;
            MouseSvcData mouseData;
            memset(&mouseData, 0, sizeof(mouseData));
            static uint8_t ax = 0;
            ax++;
            if (ax % 2 == 0) {
                mouseData.x = 1;
            } else {
                mouseData.x = 0;
            }
            mouseSvcSend(&mouseData);
        }
        mouseSvcProcess();
        __WFE();
    }
}

static void timerTimeoutHandler(void *ctx)
{
    (void)ctx;
    readMouse = true;
}

static void clockEvtHandler(nrfx_clock_evt_type_t event)
{
    switch(event) {
        case NRFX_CLOCK_EVT_LFCLK_STARTED:
            lfClkStarted = true;
            break;
    }
}
