set(APPNAME "usb_hid_mouse")
string(CONCAT TARGET ${APPNAME} ".elf")
set(LDSCRIPT "${CMAKE_CURRENT_LIST_DIR}/${APPNAME}.ld")
string(REPLACE ".elf" ".map" MAPFILE ${TARGET})
string(REPLACE ".elf" ".bin" BINARY ${TARGET})
add_executable(${TARGET})

target_include_directories(${TARGET}
    PUBLIC
        src
)

target_link_libraries(${TARGET} sdk)
target_link_options(${TARGET}
    PRIVATE
        "-Wl,--cref"
        "-Wl,-Map=${CMAKE_CURRENT_BINARY_DIR}/${MAPFILE}"
    PUBLIC
        "-T${LDSCRIPT}"
        "-L${CMAKE_CURRENT_LIST_DIR}"
)
target_sources(${TARGET}
    PUBLIC
    src/main.c
    src/imu.c
    src/mouseSvc.c
    nrf_common.ld
    ${LDSCRIPT})
set_target_properties(${TARGET} PROPERTIES LINK_DEPENDS ${LDSCRIPT})
