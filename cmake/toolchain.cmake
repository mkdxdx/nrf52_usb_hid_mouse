set (CMAKE_SYSTEM_NAME Generic)
set (CMAKE_SYSTEM_PROCESSOR arm)
set (CMAKE_C_COMPILER arm-none-eabi-gcc)
set (CMAKE_CXX_COMPILER arm-none-eabi-g++)
set (CMAKE_C_FLAGS_INIT "--specs=nosys.specs")
set (CMAKE_CXX_FLAGS_INIT "--specs=nosys.specs")
set (triple arm-none-eabi)
set (CMAKE_CROSSCOMPILING 1)

